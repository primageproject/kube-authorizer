# kube-authorizer 

`kube-authorizer` is a REST API service to automate the creation of namespaces, service accounts and permisions of users authenticated by OpenID Connect in a Kubernetes cluster. `kube-authorizer` is designed to be used alone or in combination with an [OIDC proxy](https://gitlab.com/primageproject/oidc-proxy). It should be pointed out that the Kubernetes cluster must be configured for using OpenID Connect because it is required that Kubernetes uses the `sub` claim as the username (you can see the [Kubernetes documentation](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#openid-connect-tokens) for that).

Each time `kube-authorizer` receives a request with a [ `email`, `id`/`sub` or `username`] claim, it creates a namespace for that user using the [ `email`, `id`/`sub` or `username`] if it not exists. Then, it creates a service account and the RoleBingins and the ClusterRoleBindings for the service account and the Kubernetes User. The username of the "Kubernetes User" authenticated using OIDC is defined in the kubeapi server configuration by the combination of a prefix (`--oidc-username-prefix`) and a JWT claim (`--oidc-username-claim`, `sub` is used traditionally). 


Besides, it allows the creation of other Kubernetes objects using the featura called  "generic resources". The creation of the "generic resources" is performed by the creation of an itermediate Kubernetes job, which is in charge of create the Kubernetes resources desired using the `kubectl` command (the intermediate job has this command installed). The "generic resources" can be created on any namespace (depending of the Kubernetes token used by `kube-authorizer`) and the `kube-authorizer` administrator also can configure in which namespace (user namespace or an "admin" namespace) is executed the intermediante job. An example of a generic job is the creation of a ConfigMap in the user namespace. An example of an admin generic namespace could be the creation of other object or the connection to other service that requires an administrator credential.








## Supported tags and respective Dockerfile links

* [ `latest`, `v1.1.0`  (*Dockerfile*)](https://gitlab.com/primageproject/kube-authorizer/-/blob/master/Dockerfile)

## How to use kube-authorizer

The configuration of `kube-authorizer` is performed using a yaml configuration file, which must be placed at `/etc/kube-authorizer/kube-authorizer.yaml`. This configuration file is composed of two main dictionaries: `kubernetes` and `self`. The following configuartion template is available [here](https://gitlab.com/primageproject/kube-authorizer/-/blob/master/etc/kube-authorizer.yaml.template):

```yaml
{
    "kubernetes": {
      "auth_token":  "XXXXXXXX",
      "endpoint": "https://localhost:6443",
      "oidc": {
          "user_prefix": ""
      },
      "RoleBindings" : {
          "own_namespace": [
            {
              "kind": "Role",
              "name": "my-test-role"
           }
          ],
          "other": [
            {
              "namespace": "test",
              "kind": "Role",
              "name": "my-test-role"
            }
          ]
      },
      "ClusterRoleBindings" : [
        {
          "kind": "Role",
          "name": "my-test-cluster-role"
        }
      ],
      "generic_resources": {
          "admin_namespace": "default",
          "docker_image": "primage/console_ub18:latest",
          "replace_variables_json": [ 
            '{ "replace_in_json": "__SUB__", "my_var_name": "user"}',
            '{ "replace_in_json": "__EMAIL__", "my_var_name": "email"}',
            '{ "replace_in_json": "__SERVICE_ACCOUNT_NAME__", "my_var_name": "service_account_name"}',
            '{ "replace_in_json": "__USER_ID__", "my_var_name": "user_id"}',
            '{ "replace_in_json": "__GROUPS__", "my_var_name": "groups"}',
            '{ "replace_in_json": "__OTHER__", "my_var_name": "other"}'
          ],
          "jsons_admin_namespace": [ '{....}',],
          "jsons_user_namespace": [ '{....}', ]
      }
    },
    "self": {
      "name": "kube-authorizer",
      "host": "0.0.0.0",
      "port": 10000,
      "token_rest_api_secret": "XXXXXXXXX",
      "always_check_rbac": false,
      "claim_user": "username",
      "claim_namespace": "email",
      "log": {
        "config_file": "/etc/kube-authorizer/logging.cfg",
        "level": "DEBUG",
        "file": "/var/log/kube-authorizer/kube-authorizer.log",
        "file_max_size": 10485760
      },
      "ssl": {
        "enabled": false,
        "certfile": "",
        "keyfile": ""
      }      
    }
}
```

The `kubernetes` dictionary is used to configure the connection to the Kubernetes cluster and what `kube-authorizer` has to do to authorize each user: what Role or ClusterRole bindings must create or what generic resources are required. The parameters in this dictionary are the following:
* `auth_token`: Token used for interacting with the Kubernetes cluster.  
* `endpoint`: Endpoint of the Kubernetes API.
* `oidc`: Dictionary to define other OIDC configuartions of the Kubernetes cluster.
  * `user_prefix`: String that corresponds with the prefix that Kubernetes can before the name of the each user that is authenticated using OIDC (`--oidc-username-prefix` option). 
* `RoleBindings`: Dictionary that contains the configuration of the role bindings, which are namespaced. The roles indicated here are not created by `kube-authorizer`, it use them to authorize the users. This dictionary is composed of two dictionaries:
  * `own_namespace`: List of the roles that will applied to the users in their own namespaces. Each element of the list is composed of a dictionary with the `kind` of the role (Role or ClusterRole), and the `name` of the role.  
  * `other`: List of the roles that will applied to the users in at other namespaces. Each element of the list is composed of a dictionary with the `kind` of the role (Role or ClusterRole), the `name` of the role and the `namespace` where the binding must be created . 
* `ClusterRoleBindings`: List of dictionaries that contains the ClusterRoleBindings configuration. Each element of the list is composed of a dictionary with the `kind` of the role (Role or ClusterRole), and the `name` of the role.  
* `generic_resources`: Contains the configuration of the generic resources.
  * `admin_namespace`: Name of the "admin" namespace where the admin intermediate jobs will be created. 
  * `docker_image`: Name of the docker image that will be executed as the intermediate job. This Docker image must have the `kubectl` command installed.
  * `jsons_user_namespace`: List of the Kubernetes object specifications in JSON format that will run in the user namespace.    
  * `jsons_admin_namespace`:  List of the Kubernetes object specifications in JSON format that will run in the admin namespace defined in `generic_resources.admin_namespace`.  
  * `replace_variables_json`: List of dictionaries to allow modify on the fly the JSONs defined in the previous parameters. The idea is to replace all strings in the JSON with the value of some variables. Each element is composed of the string to be replaced (`replace_in_json`) and the name of the variable that contains the value desired (`my_var_name`).  The variables that can be used are in `my_var_name` parameters are: `user` (commonly is the `sub` claim), `email`, `service_account_name`, and `user_namespace`. 


The `self` dictionary is used to configure `kube-authorizer`. The parameters are the following:
* `name`: String used in the creation of each Kubernetes object as `field_manager` (more information [here](https://kubernetes.io/docs/reference/using-api/api-concepts/#field-management).
* `host`:  Host of the REST server. Default: "0.0.0.0".
* `port`:  Port of the REST server. Default: 10000.
* `token_rest_api_secret`:  Random string to for basic authorization. 
* `always_check_rbac`:  Boolean. True if `kube-authorizer` must reload the RoleBindings and the ClusterRoleBindings each time `kube-authorizer` receives a request to configure the authorization of a user already configured. Default: false.  
* `log`: Dictionary with the configuration for logging.
  * `config_file`: path of the logging configuration file. Default: "/etc/kube-authorizer/logging.cfg",
  * `level`: level of log. Values: "DEBUG", "INFO", "WARNING", "ERROR" and "CRITICAL".
  * `file`: Path of the kube-authorizer log. Default: "/var/log/kube-authorizer/kube-authorizer.log",
  * `file_max_size`:  Maximum bytes of the log file.

Since v1.1.0, there are additional variables:
* `claim_user`: indicates if the k8s user is created using the claim `email`, `username`, or `id` (traditionally is named `sub`). Default: `username .
* `claim_namespace`: indicates which claim is used as name of the namespace: `email`, `username`, or `id` (traditionally is named `sub`). Default: `email`.


You can use the [docker image](https://gitlab.com/primageproject/kube-authorizer/container_registry) running the following code. 
```bash
docker run \
  -p $DESIRED_PORT:10000 \
  -v /path/to/kube-authorizer.yaml:/etc/kube-authorizer/kube-authorizer.yaml \
  -e PYTHONWARNINGS="ignore:Unverified HTTPS request" \
  registry.gitlab.com/primageproject/kube-authorizer:1.0.2
```
## Example: OIDC Proxy + kube-authorizer to manage the access to a  Kubernetes cluster

In this example, we will use this [OIDC proxy](https://gitlab.com/primageproject/oidc-proxy) and `kube-authorizer` to provide access to the Kubernetes Dashboard and the multitenancy in the cluster. Deployment configuration:
* Users can access and deploy resources in their namespaces and in a shared namespace called `shared`. 
* The OpenID Connect Provider used is [EGI Check-in](https://aai.egi.eu/oidc/). 
* Only the people that belongs to the PRIMAGE Virtual Organization (VO) can access to the Kubernetes cluster. 
* For each new user in the Kubernetes cluster, a new ConfigMap is created in her or his namespace and a new home directory is created in the distributed storage ([Onedata](https://onedata.org/#/home)).
* The OIDC plugin of the Kubernetes cluster is configured to use the `sub` claim as the username and `oidc:` as the prefix. It can be done adding the following lines in the command of the yaml of the kube-apiservier deployment (`/etc/kubernetes/manifests/kube-apiserver.yaml`):
```yaml
...
spec:
  containers:
  - command:
    - kube-apiserver
    ...
    - --oidc-issuer-url=https://aai.egi.eu/oidc/
    - --oidc-client-id=primage-kubernetes
    - --oidc-username-claim=sub
    - --oidc-username-prefix="oidc:"
...
```

First of all, the shared namespace must be created and policies (Role and the ClusterRole) must be defined. You can see the following file [here](https://gitlab.com/primageproject/kube-authorizer/-/blob/master/examples/roles.yml):
```yaml
---
kind: Namespace
apiVersion: v1
metadata:
  labels:
    name: shared
  name: shared
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: shared-users-role-namespace
rules:
- apiGroups: ["*"]
  resources: ["secrets","pods","pods/log","pods/exec", "jobs", "deployments", "replicasets", "daemonsets", "replicationcontrollers", "statefulsets","cronjobs", "configmaps", "container", "persistentvolumeclaims", "horizontalpodautoscalers"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
- apiGroups: ["*"]
  resources: ["events", "services", "ingresses"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: shared-users-role-cluster
rules:
- apiGroups: ["*"]
  resources: ["namespaces"]
  verbs: ["get", "list","watch"]
- apiGroups: ["*"]
  resources: ["persistentvolumes"]
  verbs: ["get", "list", "watch" ]
```
As the OIDC proxy makes requests to `kube-authorizer`,  `kube-authorizer` must be deployed before. The following yaml (available [here](https://gitlab.com/primageproject/kube-authorizer/-/blob/master/examples/kube-authorizer.yml)) contains the creation of the ConfigMap that contains the configuration file, the Deployment of `kube-authorizer` container and the network service (which is a ClusterIP service). 

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: kube-authorizer
  namespace: kubernetes-dashboard
data:
  kube-authorizer.yaml: |
    {
        "kubernetes": {
          "auth_token":  "XXXXXXXXXXXXXXXXXXXXXXX",
          "endpoint": "https://my_kubernetes_addr:6443",
          "RoleBindings" : {
              "own_namespace": [
                {
                  "kind": "ClusterRole",
                  "name": "shared-users-role-namespace"
               }
              ],
              "other": [
                {
                  "namespace": "shared",
                  "kind": "ClusterRole",
                  "name": "shared-users-role-namespace"
                }
              ]
          },
          "ClusterRoleBindings" : [
            {
              "kind": "ClusterRole",
              "name": "shared-users-role-cluster"
            }
          ],
          "oidc": {
              "user_prefix": "\"oidc:\""
          },
          "generic_resources": {
              "docker_image": "primage/console_ub18:latest",
              "admin_namespace": "default",
              "replace_variables_json": [ 
                '{ "replace_in_json": "__SUB__", "my_var_name": "user"}',
                '{ "replace_in_json": "__EMAIL__", "my_var_name": "email"}'
              ],
              "jsons_user_namespace": [ 
                '{ 
                    "kind": "ConfigMap", 
                    "apiVersion": "v1",
                    "metadata": {
                      "name": "shared-configmap"
                    },
                    "data":{                
                      "var1": "value1",
                      "var2": "value2",
                      "var3": "value3"
                    }
                  }'
                ],
              "jsons_admin_namespace": [
                '{ 
                    "apiVersion": "batch/v1",
                    "kind": "Job",
                    "metadata": {
                      "name": "create-onedata-home-__EMAIL__"
                    },
                    "spec": {
                      "template": {
                        "spec": {
                          "restartPolicy": "Never",
                          "containers": [
                            { 
                              "name": "create-onedata-home-__EMAIL__",
                              "image": "registry.gitlab.com/primageproject/containers/console_ub18:latest",
                              "imagePullPolicy": "Always",
                              "command": [ "/create_user_homes" ],
                              "args": [ "$(ONEZONE_ENDPOINT)", "$(ONEPROVIDER_ENDPOINT)", "$(ONEZONE_TOKEN)","$(SUB)", "$(DIR_PATH)" ],
                              "env": [
                                  {
                                    "name": "ONEZONE_ENDPOINT",
                                    "value": "https://datahub.egi.eu"
                                  },
                                  {
                                    "name": "ONEPROVIDER_ENDPOINT",
                                    "value": "https://upv.datahub.egi.eu"
                                  },
                                  {
                                    "name": "ONEZONE_TOKEN",
                                    "value": "XXXXXXXXXXXXXXXXXXXXXXX"
                                  },
                                  {
                                    "name": "SUB",
                                    "value": "__SUB__"
                                  },
                                  {
                                    "name": "DIR_PATH",
                                    "value": "ONEDATA_SPACE/__EMAIL__"
                                  }                      
                              ]
                            }
                          ]
                        }
                      }
                    }
                }'
              ]
          }
        },
        "self": {
          "name": "kube-authorizer",
          "host": "0.0.0.0",
          "port": 10000,
          "token_rest_api_secret": "XXXXXXXXXXXXXXXXXXXXXXX",
          "always_check_rbac": false,
          "log": {
            "config_file": "/etc/kube-authorizer/logging.cfg",
            "level": "DEBUG",
            "file": "/var/log/kube-authorizer/kube-authorizer.log",
            "file_max_size": 10485760
          }      
        }
    }
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kube-authorizer
  namespace: kubernetes-dashboard
  labels:
    app: kube-authorizer
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kube-authorizer
  template:
    metadata:
      labels:
        app: kube-authorizer
    spec:
      containers:
      - name: kube-authorizer
        image: registry.gitlab.com/primageproject/kube-authorizer:1.0.2
        imagePullPolicy: Always
        ports:
          - containerPort: 10000
            name: rest-api
        volumeMounts:
         - name: kube-authorizer
           mountPath: /etc/kube-authorizer/kube-authorizer.yaml
           subPath: kube-authorizer.yaml
        env:
         - name: PYTHONWARNINGS
           value: "ignore:Unverified HTTPS request"
      volumes:
        - name: kube-authorizer
          configMap:
            name: kube-authorizer
  
---
apiVersion: v1
kind: Service
metadata:
  namespace: kubernetes-dashboard
  name: kube-authorizer
spec:
  type: ClusterIP
  ports:
  - name: rest-api
    port: 10000
    targetPort: 10000
  selector:
    app: kube-authorizer
```

Finally, the OIDC proxy can be deployed using the following yaml (available [here](https://gitlab.com/primageproject/kube-authorizer/-/blob/master/examples/oidc_proxy.yml)), which contains the ConfigMap with the certificate, the OIDC proxy configuration file and the Kubernetes service (which is a NodePort service):
```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx
  namespace: kubernetes-dashboard
data:
  server.crt: |
    -----BEGIN CERTIFICATE-----
    ..........
    -----END CERTIFICATE-----

  server.key: |
    -----BEGIN PRIVATE KEY-----
    ..........
    -----END PRIVATE KEY-----

  config: |
    local _M = {}

    _M.opts = {
       redirect_uri_path = "/redirect_uri",
       discovery = "https://aai.egi.eu/oidc/.well-known/openid-configuration",                 
       client_id = "XXXXXXXXXXXXXXXXXX",
       client_secret = "XXXXXXXXXXXXXXXXXX",
       ssl_verify = "no",
       scope = "openid email profile eduperson_entitlement offline_access",
       redirect_uri_scheme = "https",
       refresh_session_interval = 300
    }

    _M.authorizer = {
        kube_authorizer_params = {
          endpoint = "http://kube-authorizer.kubernetes-dashboard.svc.cluster.local:10000",
          rest_api_secret = "XXXXXXXXXXXXXXXXX",
        },
        claims_required = {
          eduperson_entitlement = "urn:mace:egi.eu:aai.egi.eu:member@vo.primage.eu"
        },
    }
    return _M   
    
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: oidc-proxy-nginx
  namespace: kubernetes-dashboard
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: primage/oidc-proxy:v1.2.0-k8s-v1
        imagePullPolicy: Always
        command: ["/usr/local/openresty/bin/openresty", "-g", "daemon off;"]
        ports:
          - containerPort: 443
            name: https
          - containerPort: 80
            name: http
        env:
         - name: PROXY_HOST
           value: "KUBERNETES_DASHBOARD_HOST"
         - name: PROXY_PORT
           value: "KUBERNETES_DASHBOARD_PORT"
         - name: PROXY_PROTOCOL
           value: "https"
         - name: OID_SESSION_NAME
           value: "oidc_auth"
         - name: OID_SESSION_SECRET
           value: "XXXXXXXXXXXXXXXX"

        volumeMounts:
         - name: nginx
           mountPath: /usr/local/openresty/nginx/lua/config_vars.lua
           subPath: config
         - name: nginx
           mountPath: /usr/local/openresty/nginx/ssl/nginx.key
           subPath: server.key
         - name: nginx
           mountPath: /usr/local/openresty/nginx/ssl/nginx.crt
           subPath: server.crt
           
      volumes:
        - name: nginx
          configMap:
            name: nginx

---
apiVersion: v1
kind: Service
metadata:
  namespace: kubernetes-dashboard
  name: nginx
spec:
  type: NodePort
  ports:
  - name: https
    port: 443
    nodePort: 31443
  selector:
    app: nginx
```
## License

This project is licensed under the [Apache License 2.0](https://gitlab.com/primageproject/kube-authorizer/blob/master/LICENSE).


## Issues

If you have any problems with or questions about this image, please contact us
through a [Gitlab issue](https://gitlab.com/primageproject/kube-authorizer/-/issues).

## Contributing

You are invited to contribute new features, fixes, or updates, large or small;
we are always thrilled to receive pull requests, and do our best to process them
as fast as we can.

Before you start to code, we recommend discussing your plans through a [Gitlab
issue](https://gitlab.com/primageproject/kube-authorizer/-/issues), especially for more
ambitious contributions. This gives other contributors a chance to point you in
the right direction, give you feedback on your design, and help you find out if
someone else is working on the same thing.
