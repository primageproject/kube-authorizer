#! /usr/bin/env python3
#
# Kubernetes authorizer - PRIMAGE
# Copyright (C) 2011 - GRyCAP - Universitat Politecnica de Valencia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import logging.handlers
import logging.config
import sys
import threading
import signal
import time
import queue
import yaml

import src.RESTServer as RESTServer
from src.config import Config, default_config, check_config_items

from src import __version__ as __version__
from src.config import __VALID_CLAIMS__ as __VALID_CLAIMS__

LOGGER = logging.getLogger('kube-authorizer')
KUBE_AUTHORIZER = None
CONFIG = None #Config()

class ExtraInfoFilter(logging.Filter):
    """
    This is a filter which injects extra attributes into the log.
      * hostname
    """
    def filter(self, record):
        import socket
        record.hostname = socket.gethostname()
        return True

def config_logger():
    global CONFIG
    try:
        logging.config.fileConfig("/etc/kube-authorizer/logging.cfg")
    except Exception as ex:
        print(ex)
        log_dir = os.path.dirname(CONFIG.self.log.file)
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)

        fileh = logging.handlers.RotatingFileHandler(filename=CONFIG.self.log.file, maxBytes=CONFIG.self.log.file_max_size, backupCount=3)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fileh.setFormatter(formatter)

        if CONFIG.self.log.level == "DEBUG":
            log_level = logging.DEBUG
        elif CONFIG.self.log.level == "INFO":
            log_level = logging.INFO
        elif CONFIG.self.log.level in ["WARN", "WARNING"]:
            log_level = logging.WARN
        elif CONFIG.self.log.level == "ERROR":
            log_level = logging.ERROR
        elif CONFIG.self.log.level in ["FATAL", "CRITICAL"]:
            log_level = logging.FATAL
        else:
            log_level = logging.WARN

        logging.RootLogger.propagate = 0
        logging.root.setLevel(logging.ERROR)

        log = logging.getLogger('kube-authorizer')
        log.setLevel(log_level)
        log.propagate = 0
        log.addHandler(fileh)

    # Add the filter to add extra fields
    try:
        filt = ExtraInfoFilter()
        log = logging.getLogger('kube-authorizer')
        log.addFilter(filt)      
    except Exception as ex:
        print(ex)

        #sys.exit('Cannot read the logging configuration in '+ CONFIG.self.log_CONF_FILE)

def start_daemon():
    global KUBE_AUTHORIZER

    LOGGER.info( '------------- Starting Kubernetes Authorizer %s -------------' % __version__)

    KUBE_AUTHORIZER = RESTServer.run_in_thread(host=CONFIG.self.host, port=CONFIG.self.port, config=CONFIG)

    while KUBE_AUTHORIZER.is_alive():
        time.sleep(0.1)

    
def stop_daemon( ):
    global KUBE_AUTHORIZER
    RESTServer.stop()

    while KUBE_AUTHORIZER != None and KUBE_AUTHORIZER.is_alive():
        time.sleep(0.1)

    LOGGER.info( '------------- Kubernetes Authorizer stopped -------------' )


def signal_int_handler(signal, frame):
    """
    Callback function to catch the system signals
    """
    stop_daemon()


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_int_handler)

    c_default_config = Config (dictionary=yaml.load(default_config, Loader=yaml.Loader))            
    data = None

    try:
        with open('/etc/kube-authorizer/kube-authorizer.yaml') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
    except:
       print("ERROR: Configuration file not found or cannot be loaded in /etc/kube-authorizer/kube-authorizer.yaml")
       sys.exit(1)

    CONFIG = Config(dictionary=data)
    CONFIG.AVAILABLE_REPLACE_VARIABLES = [ 'email', 'user_namespace', 'user', 'username', 'service_account_name', 'id', 'groups', 'other' ]

    if not check_config_items(None, c_default_config.creation_dict, CONFIG.creation_dict):
        print("ERROR: Configuration file not valid")
        sys.exit(1)
    
    if CONFIG.self.claim_user not in __VALID_CLAIMS__:
        print("%s -> Invalid config.self.claim_user value. You must select one of the following: %s" % (config.self.claim_user, str(__VALID_CLAIMS__) ))
        sys.exit(1)
    if CONFIG.self.claim_namespace not in __VALID_CLAIMS__:
        print("%s -> Invalid config.self.claim_namespace value. You must select one of the following: %s" % (config.self.claim_namespace, str(__VALID_CLAIMS__) ))
        sys.exit(1)

    config_logger()
    start_daemon()