# CEM - Cluster Elasticity Manager 
# Copyright (C) 2011 - GRyCAP - Universitat Politecnica de Valencia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import yaml
import sys

__VALID_CLAIMS__ = ['email', 'id', 'username']

default_config = """
{
    "kubernetes": {
      "auth_token":  "XXXXXXXX",
      "endpoint": "https://localhost:6443",
      "RoleBindings" : {
          "own_namespace": [
          ],
          "other": [
          ]
      },
      "ClusterRoleBindings" : [
      ],
      "oidc": {
          "user_prefix": ""
      },
      "generic_resources": {
          "replace_variables_json": [],
          "admin_namespace": "default",
          "docker_image": "primage/console_ub18:latest",
          "jsons_admin_namespace": [],
          "jsons_user_namespace": []
      }
    },
    "self": {
      "name": "kube-authorizer",
      "host": "0.0.0.0",
      "port": 10000,
      "token_rest_api_secret": "XXXXXXXXX",
      "always_check_rbac": false,
      "claim_user": "username",
      "claim_namespace": "email",
      "log": {
        "config_file": "/etc/kube-authorizer/logging.cfg",
        "level": "DEBUG",
        "file": "/var/log/kube-authorizer/kube-authorizer.log",
        "file_max_size": 10485760
      },
      "ssl": {
        "enabled": false,
        "certfile": "",
        "keyfile": ""
      }      
    }
}
"""

def check_config_items (parent_key, ok_config, config):
    #global LOG
    ok = True
    for k,v in ok_config.items():
        items = config.keys()
        if k not in items:
            #print("k = %s, items = %s"%(k, str(items)))
            print("%s not defined in %s section. Required values=%s, obtained=%s"%(k, parent_key, ok_config.keys(), items)) 
            ok = False
            break
        
        if type(v) == type(config.get(k)):
            if type(v) == dict:
                ok = check_config_items(k, v, config.get(k))
        else:
            ok = False
            print("type of %s['%s'] must be %s" % (parent_key, k, type(v)) )
        if not ok:
            break   
    return ok

class Config:
    creation_dict = None
    def __init__(self, dictionary={}):
        self.creation_dict = dictionary
        for k, v in dictionary.items():
            if type(v) == dict:
                setattr(self, k, Config(v))
            else:
                setattr(self, k, v)


