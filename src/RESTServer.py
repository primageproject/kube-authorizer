#! /usr/bin/env python3
#
# Kubernetes authorizer - PRIMAGE
# Copyright (C) 2011 - GRyCAP - Universitat Politecnica de Valencia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bottle
import threading
import logging
import json
import yaml
import base64
import time

from kubernetes import client, config
from kubernetes.client.rest import ApiException

LOG = logging.getLogger('kube-authorizer')

app = bottle.Bottle()
RESTServer = None
KUBERNETES_CONFIG = None
KUBERNETES_API_CLIENT = None
CONFIG = None

class RESTServer (bottle.ServerAdapter):

    def run(self, handler):
        try:
            # First try to use the new version
            from cheroot import wsgi
            server = wsgi.Server((self.host, self.port), handler, request_queue_size=32)
        except:
            from cherrypy import wsgiserver
            server = wsgiserver.CherryPyWSGIServer((self.host, self.port), handler, request_queue_size=32)

        self.srv = server
        try:
            server.start()
        finally:
            server.stop()

    def shutdown(self):
        if self.srv:
            self.srv.stop()

def run(host, port, config):
    global RESTServer, LOG, KUBERNETES_CONFIG, KUBERNETES_API_CLIENT, CONFIG
    
    CONFIG = config

    KUBERNETES_CONFIG = client.Configuration()
    KUBERNETES_CONFIG.verify_ssl = False
    KUBERNETES_CONFIG.host = CONFIG.kubernetes.endpoint
    KUBERNETES_CONFIG.api_key = {"authorization": "Bearer " + CONFIG.kubernetes.auth_token}

    KUBERNETES_API_CLIENT = client.ApiClient(KUBERNETES_CONFIG)

    options = {}
    if CONFIG.self.ssl.enabled == True:
        if os.path.exists(CONFIG.self.ssl.certfile) and os.path.exists(CONFIG.self.ssl.keyfile):
            options['certfile'] = CONFIG.self.ssl.certfile
            options['keyfile'] = CONFIG.self.ssl.keyfile


    RESTServer = RESTServer(host=host, port=port)
    bottle.run(app, server=RESTServer, options=options, quiet=True)

def stop():
    if RESTServer:
        RESTServer.shutdown()

def run_in_thread(host, port, config):
    bottle_thr = threading.Thread(target=run, args=(host, port, config))
    bottle_thr.daemon = True
    bottle_thr.start()
    return bottle_thr
    
def check_auth (token):
    global CONFIG
    return token == CONFIG.self.token_rest_api_secret

def get_media_type(header):
    """
    Function to get specified the header media type.
    Returns a List of strings.
    """
    res = []
    accept = bottle.request.headers.get(header)
    if accept:
        media_types = accept.split(",")
        for media_type in media_types:
            pos = media_type.find(";")
            if pos != -1:
                media_type = media_type[:pos]
            if media_type.strip() in ["text/yaml", "text/x-yaml"]:
                res.append("text/yaml")
            else:
                res.append(media_type.strip())

    return res

@app.route('/', method='GET')
def RESTGetInfrastructureIsudonfo():
    LOG.debug("Received /")
    bottle.response.content_type = "text/plain"
    return "Hello from Kubernetes Authorizer"

@app.route('/authorize', method='POST')
def authorize():
    global CONFIG
   
    LOG.debug("Received /authorize")
    content_type = get_media_type('Content-Type')
    if not content_type:
        content_type = []
    
    bottle.response.content_type = "application/json"
    msg = {"changed": False, "error": False, "msg": "Unauthorized"}
    bottle.response.status = 403

    token = ""
    if 'Authorization' in bottle.request.headers:
        token = bottle.request.headers['Authorization']
    
    data = {}
    read_data = ""
    if 'application/json' in content_type:
        read_data = bottle.request.body.read().decode('UTF-8') 
        LOG.debug("read_data: "+ read_data)
        try:
            data = json.loads( read_data )
        except:
            bottle.response.status = 422
            msg["msg"] = "422 Unprocessable Entity - Cannot load the JSON sent as body. "
            msg["error"] = True
            LOG.error("Cannot read the body of the request: %s" % read_data)
    else:
        bottle.response.status = 415
        msg["msg"] = "(%s) - 415 Unsupported Media Type. Valid media types: %s "%(content_type, "application/json")
        msg["error"] = True
    

    if ( (CONFIG.self.claim_user == "email") or (CONFIG.self.claim_namespace == "email") ) and ('email' not in data.keys()):
        msg["msg"] = "422 Unprocessable Entity - Invalid JSON sent as body. Required keys: email."
        msg["error"] = True
        LOG.error("JSON does not have the required keys: %s" % read_data)
    if ( (CONFIG.self.claim_user == "username") or (CONFIG.self.claim_namespace == "username") ) and ('username' not in data.keys()):
        msg["msg"] = "422 Unprocessable Entity - Invalid JSON sent as body. Required keys: username."
        msg["error"] = True
        LOG.error("JSON does not have the required keys: %s" % read_data)
    if ( (CONFIG.self.claim_user == "id") or (CONFIG.self.claim_namespace == "id") ) and ('id' not in data.keys()):
        msg["msg"] = "422 Unprocessable Entity - Invalid JSON sent as body. Required keys: id."
        msg["error"] = True
        LOG.error("JSON does not have the required keys: %s" % read_data)

    k8s_user = None
    k8s_namespace = None
    k8s_service_account_name = None
    if not msg["error"]:
        k8s_user = data[CONFIG.self.claim_user]
        k8s_namespace = parse_k8s_names(data[CONFIG.self.claim_namespace])
        k8s_service_account_name = parse_k8s_names(k8s_user) + "-account"

    LOG.debug("k8s_user = " + k8s_user)
    LOG.debug("k8s_namespace = " + k8s_namespace)
    LOG.debug("k8s_service_account_name = " + k8s_service_account_name)


    if (not k8s_user) or (not k8s_namespace) or (not k8s_service_account_name):
        msg["msg"] = "406 Not Acceptable. User, namespace or service account name cannot be set"
        msg["error"] = True
        bottle.response.status = 406

    MY_USER_VARS = { 
        'service_account_name': k8s_service_account_name, 
        'user': k8s_user,
        'user_namespace': k8s_namespace
        }    

    for k, v in data.items():
        if k not in MY_USER_VARS.keys():
            MY_USER_VARS[ k ] = parse_k8s_names(str(v), truncate=False)
       
    LOG.debug("MY_USER_VARS: "+ json.dumps(MY_USER_VARS))

    # Check token
    if check_auth(token) and not msg["error"]:
        namespace_exists = False

        if k8s_namespace in get_all_Namespaces():
            namespace_exists = True
            bottle.response.status = 200
            msg['msg'] = "Namespace %s exists. " % (k8s_namespace)
        
        LOG.debug("Namespace exists -> " + str(namespace_exists))
        if not namespace_exists:
            if (create_Namespace(k8s_namespace)): 
                bottle.response.status = 200               
                msg['msg'] = "Namespace %s created. " % (k8s_namespace)
                msg['changed'] = True  
                namespace_exists = True                
                
                # Service account
                _changed, _error, _msg, service_account_token = create_serviceAccount(k8s_service_account_name, k8s_namespace)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error
                bottle.response.status = 200  
                LOG.debug(json.dumps(msg))

                # RoleBinding 
                _changed, _error, _msg = create_RoleBindings(k8s_user, k8s_service_account_name, k8s_namespace)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error
                LOG.debug(json.dumps(msg))

                # Create ClusterRoleBindings
                _changed, _error, _msg = create_ClusterRoleBindings(k8s_user, k8s_service_account_name, k8s_namespace)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error
                LOG.debug(json.dumps(msg))

                # Create GenericResources
                _changed, _error, _msg = create_generic_resources(k8s_user, service_account_token, k8s_namespace, MY_USER_VARS)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error
                LOG.debug(json.dumps(msg))
                
            else:
                LOG.error("%s - Cannot create namespace %s"%(k8s_user, k8s_namespace))
                msg['msg'] = "Cannot create namespace %s" % (k8s_namespace)
                msg['error'] = True

        if CONFIG.self.always_check_rbac and namespace_exists and not msg['changed']:
                # RoleBinding 
                _changed, _error, _msg = create_RoleBindings(k8s_user, k8s_service_account_name, k8s_namespace)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error

                # Create ClusterRoleBindings
                _changed, _error, _msg = create_ClusterRoleBindings(k8s_user, k8s_service_account_name, k8s_namespace)
                msg['msg']+= _msg 
                msg['changed'] = msg['changed'] or _changed
                msg['error'] = msg['error'] or _error


    return json.dumps(msg)

def parse_k8s_names(string, truncate=True):
    global LOG
    new_string = str(string).lower().replace("_","--").replace("@","-at-").replace(".","-dot-").replace('"', '' ).replace('\\', '' ).replace('..', '').replace('%', '-perc-').replace(" ","")
    if truncate:
        start = 0
        end = len(new_string)-1
        while (new_string[start].isalnum() == False) and start < end:
            start += 1 
        while (new_string[end].isalnum() == False) and end > start:
            end -= 1  
        if start == end:
            LOG.error("parse_k8s_names -> Cannot convert %s to a valid name to k8s" % (new_string))
            new_string = None
        else:  
            new_string= new_string[start:end+1] 
    
    if len(new_string) >= 63:
        new_string = new_string[:63]

    return new_string

def get_all_Namespaces():
    global LOG, KUBERNETES_API_CLIENT
    API = client.CoreV1Api(KUBERNETES_API_CLIENT)
    namespaces = [] 
    try:
        api_response = API.list_namespace(watch=False)
        LOG.debug("get_all_namespaces -> %s" % api_response)

        for namespace_info in api_response.items:
            if namespace_info.metadata.name != None:
                namespaces.append(namespace_info.metadata.name)
            elif namespace_info.metadata.self_link != None:
                namespaces.append(namespace_info.metadata.self_link.split("/")[-1])
            

    except ApiException as e:
        LOG.error("Exception when calling CoreV1Api->list_namespace: %s\n" % e)

    LOG.debug("Current namespaces: %s" % str(namespaces))
    return namespaces

def create_Namespace(name):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.CoreV1Api(KUBERNETES_API_CLIENT)
    
    try:
        api_response = API.create_namespace(client.V1Namespace(metadata=client.V1ObjectMeta(name=name)), field_manager=CONFIG.self.name )
        
        LOG.info("Namespace %s successfully created" % (name) )
        return True

    except ApiException as e:
        LOG.error("Exception when calling CoreV1Api->create_namespace: %s\n" % e)

    return False

def create_serviceAccount(name, namespace):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.CoreV1Api(KUBERNETES_API_CLIENT)
    _error = False
    _changed = False
    _msg = "Cannot create the service account %s in namespace %s. " % (name, namespace)
    _service_account_token = None
    try:
        serviceaccount = client.V1ServiceAccount(
            metadata = client.V1ObjectMeta(namespace=namespace, name=name)
        )
        api_response = API.create_namespaced_service_account(namespace, serviceaccount , field_manager=CONFIG.self.name )
        LOG.debug(api_response)
        _msg = "Service account %s successfully created in namespace %s. " % (name, namespace)
        _changed =  True
        
        retry = 0
        while _service_account_token == None and retry < 10:
            _service_account_token = get_serviceAccount_token(name, namespace)
            if _service_account_token == None:
                time.sleep(30)
                retry += 1
            else:
                break

        if retry >= 10:
            _msg ="Cannot retrieve service account token"
            _error = True

    except ApiException as e:
        LOG.error("Exception when calling CoreV1Api->create_namespaced_service_account: %s\n" % e)
        _error = True

    LOG.info(_msg)
    return _changed, _error, _msg, _service_account_token

def create_RoleBindings(user, service_account_name, namespace):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.RbacAuthorizationV1Api(KUBERNETES_API_CLIENT)
    _error = False
    _changed = False
    
    rb_created = []
    rb_error = []
    rb_already_exists = []

    current_RoleBindings = list_RoleBindings(namespace)

    for role in CONFIG.kubernetes.RoleBindings.own_namespace:
        try:
            role_name = role['name'].replace(' ','')
            role_kind = role['kind']
        except ValueError as e:
            LOG.error("Role has not the required paramenters: name and kind") 
        
        role_binding_name = parse_k8s_names("user-"+user+"-role-"+ role_name)

        role_binding_user =  CONFIG.kubernetes.oidc.user_prefix + user
        
        if role_binding_name not in current_RoleBindings:
            role_binding = client.V1RoleBinding(
                metadata = client.V1ObjectMeta(namespace=namespace, name=role_binding_name),
                subjects = [ 
                    client.V1Subject(name= role_binding_user , kind="User", api_group="rbac.authorization.k8s.io", namespace=namespace),
                    client.V1Subject(name= service_account_name , kind="ServiceAccount", namespace=namespace)],
                role_ref = client.V1RoleRef(kind=role_kind, api_group="rbac.authorization.k8s.io", name=role_name)
                )
            try:
                api_response = API.create_namespaced_role_binding(namespace, body=role_binding , field_manager=CONFIG.self.name)
                rb_created.append(role_binding_name)
                LOG.debug("\tRoleBinging (%s , %s) -> %s" % (user, role_binding_name, api_response))   
            except ApiException as e:
                LOG.error("Exception when calling RbacAuthorizationV1Api->create_namespaced_role_binding: %s\n" % e) 
                rb_error.append(role_binding_name)
                _error=True
        else:
            rb_already_exists.append(role_binding_name)

    for role in CONFIG.kubernetes.RoleBindings.other:
        try:
            role_name = role['name']
            role_kind = role['kind']
            role_namespace = role['namespace']
        except ValueError as e:
            LOG.error("Role has not the required paramenters: name, kind and namespace. ")
        role_binding_name = parse_k8s_names("user-"+user+"-role-"+ role_name)
        role_binding_user =  CONFIG.kubernetes.oidc.user_prefix + user
        current_RoleBindings = list_RoleBindings(role_namespace)

        if role_binding_name not in current_RoleBindings:     
            role_binding = client.V1RoleBinding(
                metadata = client.V1ObjectMeta(namespace=role_namespace, name=role_binding_name),
                subjects = [ 
                    client.V1Subject(name= role_binding_user , kind="User", api_group="rbac.authorization.k8s.io", namespace=role_namespace)
                    ],
                role_ref = client.V1RoleRef(kind=role_kind, api_group="rbac.authorization.k8s.io", name=role_name)
                )
            try:
                api_response = API.create_namespaced_role_binding(role_namespace, body=role_binding , field_manager=CONFIG.self.name)
                rb_created.append(role_binding_name)
                LOG.debug("\tRoleBinging (%s , %s) -> %s" % (user, role_binding_name, api_response))   
            except ApiException as e:
                LOG.error("Exception when calling RbacAuthorizationV1Api->create_namespaced_role_binding: %s\n" % e) 
                rb_error.append(role_binding_name)
                _error=True
        else:
            rb_already_exists.append(role_binding_name)

    if len(rb_created)>0:
        _changed=True

    _msg = "RoleBindings -> created=%s, with_error=%s, already_created=%s. " % (str(rb_created), str(rb_error), str(rb_already_exists))
    LOG.info(user + " --> " +_msg)
    return _changed, _error, _msg

def create_ClusterRoleBindings(user,service_account_name, namespace):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.RbacAuthorizationV1Api(KUBERNETES_API_CLIENT)
    _changed = False
    _error = False
    
    crb_created = []
    crb_error = []
    crb_already_exists = []

    current_ClusterRoleBindings = list_ClusterRoleBindings()

    for role in CONFIG.kubernetes.ClusterRoleBindings:
        try :
            role_name = role['name'].replace(' ','')
            role_kind = role['kind']
        except ValueError as e:
            LOG.error("Role has not the required paramenters: name, kind") 
        
        role_binding_name = parse_k8s_names("user-"+user+"-role-"+ role_name)
        role_binding_user =  CONFIG.kubernetes.oidc.user_prefix + user 

        if role_binding_name not in current_ClusterRoleBindings:
            cluster_role_binding = client.V1ClusterRoleBinding(
                metadata = client.V1ObjectMeta(name=role_binding_name),
                subjects = [ 
                    client.V1Subject(name= role_binding_user , kind="User", api_group="rbac.authorization.k8s.io"),
                    client.V1Subject(name= service_account_name , kind="ServiceAccount", namespace=namespace) 
                    ],
                role_ref = client.V1RoleRef(kind=role_kind, api_group="rbac.authorization.k8s.io", name=role_name)
                )
            try:
                api_response = API.create_cluster_role_binding(body=cluster_role_binding , field_manager=CONFIG.self.name)
                crb_created.append(role_binding_name)
                LOG.debug("\tClusterRoleBinging (%s , %s) -> %s" % (user, role_binding_name, api_response))   
            except ApiException as e:
                LOG.error("Exception when calling RbacAuthorizationV1Api->create_cluster_role_binding: %s\n" % e) 
                crb_error.append(role_binding_name)
                _error=True
        else:
            crb_already_exists.append(role_binding_name)

    if len(crb_created)>0:
        _changed=True

    _msg = "ClusterRoleBindings -> created=%s, with_error=%s, already_created=%s. " % (str(crb_created), str(crb_error), str(crb_already_exists))
    LOG.info(user + " --> " +_msg)
    return _changed, _error, _msg

def list_RoleBindings(namespace):
    global LOG, KUBERNETES_API_CLIENT, CONFIG

    API = client.RbacAuthorizationV1Api(KUBERNETES_API_CLIENT)
    rolebinding= []
    try:
        api_response = API.list_namespaced_role_binding(namespace, watch=False)
        #LOG.debug("list_namespaced_role_binding -> %s" % api_response)

        for rb_info in api_response.items:
            rolebinding.append(rb_info.metadata.name)

    except ApiException as e:
        LOG.error("Exception when calling RbacAuthorizationV1Api->list_namespaced_role_binding: %s\n" % e) 

    LOG.debug("Current RoleBindings: %s" % str(rolebinding))

    return rolebinding

def list_ClusterRoleBindings():
    global LOG, KUBERNETES_API_CLIENT, CONFIG

    API = client.RbacAuthorizationV1Api(KUBERNETES_API_CLIENT)
    clusterrolebinding= []
    try:
        api_response = API.list_cluster_role_binding(watch=False)
        #LOG.debug("list_cluster_role_binding -> %s" % api_response)

        for crb_info in api_response.items:
            clusterrolebinding.append(crb_info.metadata.name)

    except ApiException as e:
        LOG.error("Exception when calling RbacAuthorizationV1Api->list_cluster_role_binding: %s\n" % e) 

    LOG.debug("Current ClusterRoleBindings: %s" % str(clusterrolebinding))

    return clusterrolebinding

def get_serviceAccount_token(service_account_name, namespace):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.CoreV1Api(KUBERNETES_API_CLIENT)
    token = None
    try:
        response = API.list_namespaced_secret(namespace)
        #LOG.debug("Response when getting service account token: %s\n" % response)
        for secret in response.items:
            saccount = secret.metadata.annotations['kubernetes.io/service-account.name']
            if service_account_name == saccount:
                token = base64.b64decode(secret.data['token']).decode("utf-8")

    except ApiException as e:
        LOG.error("Exception when calling CoreV1Api->list_namespaced_secret: %s\n" % e)

    if token is None:
        LOG.error("Cannot obtain token for sevice account %s" % service_account_name)
    
    return token

def create_generic_resources(user, service_account_token, namespace_name, MY_USER_VARS):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    _changed = False
    _error = False

    j_created = []
    j_error = []

    
    for rjson in CONFIG.kubernetes.generic_resources.jsons_user_namespace:
        _rjson = None
        ok = False
        resource_name = "name-cannot-be-loaded"
        try:
            for variable_to_replace_str in CONFIG.kubernetes.generic_resources.replace_variables_json:
                replace_in_json = ''
                my_var_name = ''
                try:
                    data = json.loads( variable_to_replace_str )
                    replace_in_json = data['replace_in_json'] 
                    my_var_name = data['my_var_name'] 
                except:
                    LOG.error( "%s is not valid to replace. Check the template provided in the repository, each element must contain the following keys: replace_in_json, my_var_name." % (variable_to_replace_str) )

                if my_var_name in CONFIG.AVAILABLE_REPLACE_VARIABLES:
                    rjson=rjson.replace(replace_in_json, MY_USER_VARS[my_var_name])
                else:
                    LOG.warning( "%s is not valid to replace. You only can use %s" % (my_var_name, str(CONFIG.AVAILABLE_REPLACE_VARIABLES) ) )
            _rjson = json.loads( rjson )
        except:
            LOG.error("Error parsing the json defined in kubernetes.generic_resources.jsons_user_namespace: %s" % (rjson))
        
        if _rjson is not None and service_account_token is not None:
            resource_name = parse_k8s_names( search_attr_dict(_rjson, "name") )
            if resource_name is None:
                resource_name = "noname"
            metadata_name = parse_k8s_names("creating-" + resource_name)
            if len(metadata_name)>=63:
                metadata_name=metadata_name[:63]
            metadata = client.V1ObjectMeta(namespace=namespace_name, name=metadata_name )

            ok = create_Job(namespace_name, service_account_token, metadata, rjson, resource_name)

        if ok:
            j_created.append(resource_name)
            _changed = True
        else:
            j_error.append(resource_name)
            _error = True

        
    for rjson in CONFIG.kubernetes.generic_resources.jsons_admin_namespace:
        _rjson = None
        ok = False
        resource_name = "name-cannot-be-loaded"
        try:
            for variable_to_replace_str in CONFIG.kubernetes.generic_resources.replace_variables_json:
                replace_in_json = ''
                my_var_name = ''
                
                try:
                    data = json.loads( variable_to_replace_str )
                    replace_in_json = data['replace_in_json'] 
                    my_var_name = data['my_var_name'] 
                except:
                    LOG.error( "%s is not valid to replace. Check the template provided in the repository, each element must contain the following keys: replace_in_json, my_var_name." % (variable_to_replace_str) )

                if my_var_name in CONFIG.AVAILABLE_REPLACE_VARIABLES and my_var_name in MY_USER_VARS:
                    rjson=rjson.replace(replace_in_json,  MY_USER_VARS[my_var_name])
                else:
                    LOG.warning( "%s is not valid to replace. You only can use %s" % (my_var_name, str(CONFIG.AVAILABLE_REPLACE_VARIABLES) ) )
 
            _rjson = json.loads( rjson )
        except:
            LOG.error("Error parsing the json defined in kubernetes.generic_resources.jsons_admin_namespace: %s" % (rjson))
        
        if _rjson is not None:
            resource_name = parse_k8s_names( search_attr_dict(_rjson, "name") )
            if resource_name is None:
                resource_name = "noname"
            metadata_name = parse_k8s_names("creating-" + resource_name)
            if len(metadata_name)>=63:
                metadata_name=metadata_name[:63]
            metadata = client.V1ObjectMeta(namespace=CONFIG.kubernetes.generic_resources.admin_namespace, name=metadata_name )
            ok = create_Job(CONFIG.kubernetes.generic_resources.admin_namespace, CONFIG.kubernetes.auth_token, metadata, rjson, resource_name)

        if ok:
            j_created.append(resource_name)
            _changed = True
        else:
            j_error.append(resource_name)
            _error = True

    _msg = "Generic resources for user %s -> created=%s, with_error=%s . " % (user, str(j_created), str(j_error) )
    LOG.info(_msg)

    return _changed, _error, _msg

def create_Job(namespace, token, _metadata, resource, resource_name):
    global LOG, KUBERNETES_API_CLIENT, CONFIG
    API = client.BatchV1Api(KUBERNETES_API_CLIENT)
    _changed = False
    _error = False
    job_name = parse_k8s_names("creating-" + resource_name)
    if len(job_name) >= 63:
        job_name = job_name[0:63]
    body = client.V1Job(
        metadata = _metadata,
        spec = client.V1JobSpec(
            template = client.V1PodTemplateSpec(
                spec = client.V1PodSpec(
                    containers = [ 
                        client.V1Container(
                            image = CONFIG.kubernetes.generic_resources.docker_image,
                            name = job_name, 
                            image_pull_policy = "Always",
                            command = [ "bash" ], 
                            args = ["-c", "printenv K8S_RESOURCES_JSON | /usr/bin/kubectl --server $$K8S_ENDPOINT --insecure-skip-tls-verify=true --token=$$K8S_TOKEN --namespace=$$MY_NAMESPACE apply -f -" ],
                            env = [
                                client.V1EnvVar(name = "K8S_ENDPOINT",value = CONFIG.kubernetes.endpoint), 
                                client.V1EnvVar(name = "K8S_TOKEN", value = token), 
                                client.V1EnvVar(name = "MY_NAMESPACE", value = namespace ),
                                client.V1EnvVar(name = "K8S_RESOURCES_JSON", value = resource )                                     
                            ]
                        )
                    ],
                    restart_policy = "Never"
                )
                
            )
        ) 
    )

    try:
        api_response = API.create_namespaced_job(namespace, body, field_manager=CONFIG.self.name)
        LOG.debug(api_response)
    except ApiException as e:
        LOG.error("Exception when calling BatchV1Api->create_namespaced_job: %s\n" % e)
        return False

    return True
    
def search_attr_dict(d, atr):
    value = None
    if atr in d:
        return d[atr]
    for k, v in d.items():
        if type(v) == dict:
            value = search_attr_dict(v, atr)
            if value is not None:
                break
    return value

