#! /usr/bin/env python3
#
# Kubernetes authorizer - PRIMAGE
# Copyright (C) 2011 - GRyCAP - Universitat Politecnica de Valencia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from src import __version__ as version
from src import __author__ as author
from src import __email__ as email


setup(name='kube-authorizer',
    version=version,
    description='Kubernetes authorizer',
    author=author,
    author_email=email,
    url='https://gitlab.com/primagepoject/kube-authorizer',
    packages = [ 'src' ],
    platforms=["any"],
    data_files = [
        ('/etc/kube-authorizer', [ 
            'etc/kube-authorizer.yaml',
            'etc/kube-authorizer.yaml.template',
            'etc/logging.cfg'
        ]),
        ('/etc/init.d', [
            'service/kube-authorizer'
        ]),
        ( '/usr/bin', [
            'kube-authorizer-service.py'
        ]),
        ( '/var/log/kube-authorizer', [

        ])
    ],
    scripts=['kube-authorizer-service.py', 'service/kube-authorizer'],
    install_requires = [ "bottle", "requests", "cherrypy", "kubernetes", "pyyaml"]
)