FROM ubuntu:22.04
LABEL MAINTAINER=serlohu@upv.es
LABEL version = '1.1.2'

RUN apt-get update && \
    apt-get install --no-install-recommends -y python3 python3-pip  && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

RUN pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir setuptools 

RUN mkdir /kube-authorizer
COPY ./src/ /kube-authorizer/src
COPY ./etc/ /kube-authorizer/etc
COPY ./service/ /kube-authorizer/service
COPY kube-authorizer-service.py /kube-authorizer/kube-authorizer-service.py 
COPY setup.py /kube-authorizer/setup.py


RUN cd /kube-authorizer && python3 setup.py install 

CMD kube-authorizer-service.py 

EXPOSE 10000